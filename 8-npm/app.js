const validator = require('validator');
const chalk = require('chalk')

console.log(validator.isEmail('gui'))// false;
console.log(validator.isEmail('gui@mail.com')) // true
console.log(validator.isEmail('gui@com'))// false
console.log(validator.isEmail('gui@xcv.xx'))// true
console.log(validator.isEmail('gui@xcv.m'))// false
console.log(validator.isMobilePhone('234123423','id-ID'))// false
console.log(validator.isMobilePhone('0812345678984985','id-ID'))// false
console.log(validator.isMobilePhone('0812205059','id-ID'))// true
console.log(validator.isMobilePhone('0800205059','id-ID'))// false
console.log(validator.isNumeric('0812205059'))// true
console.log(validator.isNumeric('0812205059s'))// false

console.log(chalk.cyan.bgBlack.italic('Hello')+'world')
const lorem = chalk`Colors are {bold.bgBlue.white downsampled from} 16 million RGB values to an {bgGreen.black.bold ANSI} color format `
console.log(lorem)