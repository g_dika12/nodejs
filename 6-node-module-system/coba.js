const cetakNama = (name) => `My name is ${name}`
const data = ['satu', 'dua', 'tiga'];
// module.exports = cetakNama; // berlaku bila yg diexport hanya 1
const mahasiswa = {
    name: 'puput',
    age: 32,
    detail() {
        return `nama saya adalah ${this.name} dan saya ${this.age}`
    }
}
class Orang {
    constructor() {
        console.log('Data telah dibuat')
    }
}
// module.exports.cetakNama = cetakNama;
// module.exports.data = data;
// module.exports.mahasiswa = mahasiswa;
// module.exports.Orang = Orang;

// module.exports = {
//     cetakNama: cetakNama,
//     data: data,
//     mahasiswa: mahasiswa,
//     Orang: Orang
// }

module.exports = {
    cetakNama,
    data,
    mahasiswa,
    Orang
}