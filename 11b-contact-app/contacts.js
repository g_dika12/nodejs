const chalk = require('chalk');
const fs = require('fs');
const readline = require('readline');
const { default: validator } = require('validator');

const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
})

// membuat folder jika belum ada
const dirPath = './data'
if (!fs.existsSync(dirPath)) {
    fs.mkdirSync(dirPath)
}

// membuat file jika tidak ada
const filePath = './data/contacts.json';
if (!fs.existsSync(filePath)) {
    fs.writeFileSync(filePath, '[]', 'utf-8') // data JSON wajib string
}

// ASYNC AWAIT mengatasi CALLBACK HELL
// syarat PROMISE lebih dahulu

//PROMISE
const pertanyaan1 = () => {
    return new Promise((resolve, reject) => {
        rl.question("Masukkan nama Anda: ", (name) => {
            resolve(name)
        })
    })
}

const pertanyaan2 = () => {
    return new Promise((resolve, reject) => {
        rl.question("Masukkan email Anda: ", (email) => {
            resolve(email)
        })
    })
}

const pertanyaan3 = () => {
    return new Promise((resolve, reject) => {
        rl.question("Masukkan umur Anda: ", (age) => {
            resolve(age)
        })
    })
}

const pertanyaan4 = () => {
    return new Promise((resolve, reject) => {
        rl.question("Masukkan pekerjaan Anda: ", (job) => {
            resolve(job)
        })
    })
}


const detailQuestion = (question) => {
    return new Promise((resolve, reject) => {
        rl.question(`${question}: `, (arguments) => {
            resolve(arguments)
        })
    })
}

const loadContact = () => {
    const contactJSON = fs.readFileSync('./data/contacts.json', 'utf-8');//"[]"
    const newArray = JSON.parse(contactJSON)// create [] or catch data lama dan initial
    return newArray;
}

const mainFunction = (name, email, noHP, age, job, ayah, ibu, address, vehicle) => {
    // const contactJSON = fs.readFileSync('./data/contacts.json', 'utf-8');//"[]""
    // const newArray = JSON.parse(contactJSON); // create [] catch data lama dan initial
    const newArray = loadContact()

    //CEK DUPLIKAT 
    const duplikat = newArray.find((contact) => contact.name === name); // false atau undefined bila tidak ada nama sama
    if (duplikat) { // terdapat data sama
        console.log(chalk.bgRed.white.inverse.bold(`Contact sudah terdafat, silahkan gunakan nama yang lain`))
        rl.close() // 
        return false; // agar fungsi di bawah ini berhenti
    }

    // CEK EMAIL
    if (email) { // email = optional bukan mandatory
        if (!validator.isEmail(email)) {
            console.log(chalk.bgRed.white.inverse.bold(`Email tidak valid!`))
            rl.close() // 
            return false; // agar fungsi di bawah ini berhenti
        }
    }

    // CEK NO HP == tapi hanya bisa cek 3 - 4 awalan no hp negara
    if (!validator.isMobilePhone(noHP, 'id-ID')) {
        console.log(chalk.bgRed.white.inverse.bold(`No Hp tidak valid!`))
        rl.close() // 
        return false; // agar fungsi di bawah ini berhenti
    }

    const user = {
        name,
        email,
        noHP,
        age,
        job,
        detail: {
            ayah,
            ibu,
            address,
            vehicle
        }
    }

    newArray.push(user)
    // buat array kosong -> JSON.PARSE
    // push user ke array kosong
    // replace data lama dengan file array baru dan konvert ke string JSON.STRINGIFY
    console.log(newArray)
    fs.writeFileSync('./data/contacts.json', JSON.stringify(newArray), 'utf-8');

    console.log(chalk.bgGreen.white.bold('Terima kasih telah menginputkan data'))
    rl.close()
}
const listContact = () => {
    const contactArr = loadContact();
    console.log(chalk.cyan.inverse.bold('Daftar Kontak User: '))
    contactArr.forEach((data, i) => {
        if (data.detail.ibu) {
            console.log(i + 1, ' ', data.name, ' ++ ', data.job + ' - ', data.detail.ayah)
        }
    })
    rl.close()
}
const detailContact = (nama) => {

    const contactArr = loadContact(nama);
    // get data berdasar name dan wajib sama karakter
    // samain parameter dengan data array

    const namaUser = contactArr.find(
        (data) => {
            return data.name.toUpperCase() === nama.toUpperCase();
        }
    ); //  sukses nampilkan seluruh data detail berdasar nama
    // console.log('= ', namaUser)

    if (!namaUser) { // jika datanya = false atau undefined
        console.log(chalk.bgRed.white.inverse.bold(`${nama} tidak terdaftar!`))
        rl.close()
        return false
    } else {
        console.log(' === ', chalk.bgCyan.black(namaUser.name))
        console.log('age = ', namaUser.age)
        if (namaUser.email) {
            console.log('mail = ', namaUser.email)
        }
        rl.close()
    }
}

const deleteContact = (nama) => {
    const contactArr = loadContact();
    console.log(chalk.green.inverse.bold('Nama User yang akan dihapus: '));

    const dataArray = contactArr.filter((data) => data.name.toUpperCase() !== nama.toUpperCase()); // ARRAY baru
    // data array baru selain dta contactArr 
    
    if (dataArray.length === contactArr.length) { // jika datanya = false atau undefined
        console.log(chalk.bgRed.white.inverse.bold(`nama ${nama} tidak ditemukan!`))
        rl.close()
        return false
    } else {
        // console.log(filePath)
        fs.writeFileSync(filePath,JSON.stringify(dataArray),'utf-8')
        console.log(chalk.cyan.inverse.bold(`data contact ${nama} berhasil dihapus`))
        rl.close()
    }
    // 
    rl.close();
}

module.exports = {
    pertanyaan1,
    pertanyaan2,
    pertanyaan3,
    pertanyaan4,
    detailQuestion,
    mainFunction,
    listContact,
    detailContact,
    deleteContact
}