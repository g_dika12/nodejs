const http = require('http');

http
.createServer((request, result)=>{
  console.log('== ',request)
  const url = request.url;
  console.log(url)
    result.writeHead(200, {
      "Content-Type":'text/html'
    })
    result.write('Hello world')
    result.end();
})
.listen(3000,()=>{
    console.log('server is listening on port 3000...')
})