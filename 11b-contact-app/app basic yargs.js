// console.log(process.argv)
/*
[
  '/usr/local/Cellar/node/15.7.0/bin/node',
  '/Users/guitherezsinaga/Documents/iCloud Drive-(Archive)/Documents/DEVELOPMENT/NODE/nodejs/contact-app/app'
]
*/

//1. npm init -y
// tuk smua pertanyaan -> jawabannya yes atau default // dan akan install package.json

const yargs = require('yargs')
// console.log(yargs)

/*
[Function: Argv] {
  middleware: [Function: bound ],
  scriptName: [Function: bound ],
  '$0': [Getter],
  parsed: [Getter],
  getContext: [Function: bound ],
  exit: [Function: bound ],
  completion: [Function: bound ],
  reset: [Function: bound resetOptions],
  resetOptions: [Function: bound resetOptions],
  boolean: [Function: bound ],
  array: [Function: bound ],
  number: [Function: bound ],
  normalize: [Function: bound ],
  count: [Function: bound ],
  string: [Function: bound ],
  requiresArg: [Function: bound ],
  skipValidation: [Function: bound ],
  nargs: [Function: bound ],
  choices: [Function: bound ],
  alias: [Function: bound ],
  defaults: [Function: bound ],
  default: [Function: bound ],
  describe: [Function: bound ],
  demandOption: [Function: bound demandOption],
  coerce: [Function: bound ],
  config: [Function: bound config],
  example: [Function: bound ],
  command: [Function: bound ],
  commandDir: [Function: bound ],
  require: [Function: bound demand],
  required: [Function: bound demand],
  demand: [Function: bound demand],
  demandCommand: [Function: bound demandCommand],
  getDemandedOptions: [Function: bound ],
  getDemandedCommands: [Function: bound ],
  deprecateOption: [Function: bound deprecateOption],
  getDeprecatedOptions: [Function: bound ],
  implies: [Function: bound ],
  conflicts: [Function: bound ],
  usage: [Function: bound ],
  epilog: [Function: bound ],
  epilogue: [Function: bound ],
  fail: [Function: bound ],
  onFinishCommand: [Function: bound ],
  getHandlerFinishCommand: [Function: bound ],
  check: [Function: bound ],
  global: [Function: bound global],
  pkgConf: [Function: bound pkgConf],
  parse: [Function: bound parse],
  _getParseContext: [Function: bound ],
  _hasParseCallback: [Function: bound ],
  options: [Function: bound option],
  option: [Function: bound option],
  getOptions: [Function: bound ],
  positional: [Function: bound ],
  group: [Function: bound group],
  getGroups: [Function: bound ],
  env: [Function: bound ],
  wrap: [Function: bound ],
  strict: [Function: bound ],
  getStrict: [Function: bound ],
  strictCommands: [Function: bound ],
  getStrictCommands: [Function: bound ],
  strictOptions: [Function: bound ],
  getStrictOptions: [Function: bound ],
  parserConfiguration: [Function: bound parserConfiguration],
  getParserConfiguration: [Function: bound ],
  showHelp: [Function: bound ],
  version: [Function: bound version],
  help: [Function: bound addHelpOpt],
  addHelpOpt: [Function: bound addHelpOpt],
  showHidden: [Function: bound addShowHiddenOpt],
  addShowHiddenOpt: [Function: bound addShowHiddenOpt],
  hide: [Function: bound hide],
  showHelpOnFail: [Function: bound showHelpOnFail],
  exitProcess: [Function: bound ],
  getExitProcess: [Function: bound ],
  showCompletionScript: [Function: bound ],
  getCompletion: [Function: bound ],
  locale: [Function: bound ],
  updateLocale: [Function: bound ],
  updateStrings: [Function: bound ],
  detectLocale: [Function: bound ],
  getDetectLocale: [Function: bound ],
  _getLoggerInstance: [Function: bound ],
  _hasOutput: [Function: bound ],
  _setHasOutput: [Function: bound ],
  recommendCommands: [Function: bound ],
  getUsageInstance: [Function: bound ],
  getValidationInstance: [Function: bound ],
  getCommandInstance: [Function: bound ],
  terminalWidth: [Function: bound ],
  argv: [Getter],
  _parseArgs: [Function: bound parseArgs],
  _postProcess: [Function: bound ],
  _copyDoubleDash: [Function: bound ],
  _parsePositionalNumbers: [Function: bound ],
  _runValidation: [Function: bound runValidation]
}
*/

// console.log(' DATA YARGS => ',yargs['argv']) // kelola argument

/*
yargs -> interactive command line tools
  by parsing arguments 
  
*/
// terminal ----?> node app add --name="Dika" // nambah data dika
// { _: [ 'add' ], name: 'Dika', '$0': 'app' }

// node app remove --name="Dika"
// node app list

// belajar command yargs

//.command(cmd, desc, [builder], [handler])

yargs.command(
  'add',
  'PEROLEH DATA BARU',
  () => {/* Bisa object / fungsi */ },
  (argv) => { console.log('---?>',argv) } // argv.name ..
  /* data yang ditangkap yargs */
)

yargs.parse();
// ---?> { _: [ 'add' ], name: 'Ando', '$0': 'app' }

