const contact = require('./contacts')
// OBJECT DESTRUCTURING
// const {pertanyaan1, pertanyaan2, pertanyaan3,mainFunction, pertanyaan4, detailQuestion} = require('./contacts')
// agar await pertanyaan1() dll

// ASYNC AWAIT
const main = async () => {
    const name = await contact.pertanyaan1();
    const email = await contact.pertanyaan2();
    const age = await contact.pertanyaan3();
    const job = await contact.pertanyaan4();
    const ayah = await contact.detailQuestion('Nama ayah anda');
    const ibu = await contact.detailQuestion('Nama ibu anda');
    const address = await contact.detailQuestion('Tempat tinggal tetap');
    const vehicle = await contact.detailQuestion('Kendaraan utama keluarga');

    contact.mainFunction(name, email, age, job, ayah, ibu, address, vehicle)
    
}
main();