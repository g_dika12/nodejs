const yargs = require('yargs')
const { mainFunction, listContact, detailContact, deleteContact } = require('./contacts')

// COMMAND YARGS
console.log(`.command(cmd, desc, [builder], [handler])`)
yargs.command({
  command: 'add',
  describe: "Menambahkan data",
  builder: {
    nama: {
      describe: "Nama Lengkap",
      demandOption: true, // mandatory
      type: 'string'
    },
    email: {
      describe: "Alamat Email",
      demandOption: false, // optional
      type: 'string'
    },
    noHP: {
      describe: "Nomor Handphone",
      demandOption: true,
      type: 'string'
    },
    age: {
      describe: "Usia Anda",
      demandOption: true,
      type: 'string'
    },
    job: {
      describe: "Pekerjaan Anda",
      demandOption: true,
      type: 'string'
    },
    ayah: {
      describe: "Nama Ayah Anda",
      demandOption: true,
      type: 'string'
    },
    ibu: {
      describe: "Nama Ibu Anda",
      demandOption: true,
      type: 'string'
    },
    address: {
      describe: "Alamat tetap",
      demandOption: true,
      type: 'string'
    },
    vehicle: {
      describe: "Kendaraan utama",
      demandOption: true,
      type: 'string'
    },
  },
  handler(argv) { // data properti yang diterima yargs
    // console.log('== ', argv)
    mainFunction(
      argv.nama,
      argv.email,
      argv.noHP,
      argv.age,
      argv.job,
      argv.ayah,
      argv.ibu,
      argv.address,
      argv.vehicle,
    )
  }
})
/*
node app add
app add

Menambahkan data

Options:
  --help     Show help                         [boolean]
  --version  Show version number               [boolean]
  --nama     Nama Lengkap            [string] [required]
  --email    Alamat Email                       [string]
  --noHP     Nomor Handphone         [string] [required]


  node app help
  app add

Menambahkan data

Options:
  --help     Show help                         [boolean]
  --version  Show version number               [boolean]
  --nama     Nama Lengkap            [string] [required]
  --email    Alamat Email                       [string]
  --noHP     Nomor Handphone         [string] [required]

  node app add --nama="IST" --email="mail@xmp.ma" --noHP="0812345667"

  console.log('== ', argv)
  {
  _: [ 'add' ],
  nama: 'IST',
  email: 'mail@xmp.ma',
  noHP: '0812345667',
  'no-h-p': '0812345667',
  '$0': 'app'
}

{ contact: { name: 'IST', email: 'mail@xmp.ma', noHP: '0812345667' } }

console.log(contact)
{ contact: { name: 'IST', email: 'mail@xmp.ma', noHP: '0812345667' } }
*/

yargs.command({
  command: 'list',
  describe: 'Menampilkan List User - Nama dan Hp',
  handler() {
    // mengaktifkan fungsi list
    listContact()
  }
})
yargs.command({
  command: 'detail',
  describe: "Menampilkan detail sebuah contact berdasarkan nama",
  builder: {
    nama: {
      describe: 'Nama Lengkap',
      demandOption: true,
      type: 'string'
    }
  },
  handler(argv) {
    // panggil detail kpntak
    detailContact(argv.nama)
  }
})
yargs.command({
  command: 'delete',
  describe: "Menghapus data contact berdasarkan nama",
  builder: {
    nama: {
      describe: 'Nama User',
      demandOption: true,
      type: 'string'
    }
  },
  handler(argv) {
    // panggil detail kpntak
    deleteContact(argv.nama)
  }
})
yargs.parse();