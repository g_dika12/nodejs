// var http = require('http')
// var fs = require('fs')
// var port = 3000

// const renderHTML = (urlPath, data) => {
//     fs.readFile(urlPath, (err, response) => {
//         if (err) {
//             data.writeHead(400)
//             data.write('Error: the url not found')
//         } else {
//             data.write(response)
//         }
//         data.end()
//     })
// }
// http
//     .createServer((req, res) => {
//         const url = req.url;
//         res.writeHead(200, {
//             'Content-Type': "text/html"
//         })
//         if (url === '/') {
//             renderHTML('./index.html', res)
//         } else if (url == '/about') {
//             renderHTML('./index.html', res)
//         } else if (url == '/contact') {
//             renderHTML('./index.html', res)
//         } else if (url == '/profile') {
//             renderHTML('./index.html', res)
//         } else {
//             res.write('<h1>Welcome Home</h1>')
//         }
//     })
//     .listen(3000, () => {
//         console.log('server run on port 3000..')
//     })


// run localhost
// website NPM / search express
// npm install express@4.17.1 --save
// node_modules exists;
// package.json - updated = "dependencies" 

// START EXPRESS
// https://expressjs.com/en/starter/installing.html
// https://expressjs.com/en/starter/hello-world.html
// scaffold = rangka
// numerous = banyak sekali
// mounts = memasang / memanggil
// invoke = diminta/ mohon

const express = require('express')
const app = express();
const port = 3000;

// "/" = root
// app.get('/', (req, response) => {
//     response.send('Hello My confustucated world')
//     // === response.write di screen
// })

app.get('/myJson', (req, res) => {
    res.json({
        "name": "Dika Sinaga",
        "age": "23",
        "email": "mail@example.com"
    })
})
app.get('/', (req, res) => {
    res.sendFile('./index.html', { root: __dirname })
})
app.get('/about', (req, response) => {
    // response.send('Hello My confustucated world ABOUT')
    response.sendFile('./about.html', { root: __dirname })
    // === response.write di screen
})
app.get('/contact', (req, res) => {
    res.sendFile('./contact.html', { root: __dirname })
})
app.get('/profile', (req, response) => {
    response.send('Hello My confustucated world PROFILE')
    // === response.write di screen
})
//REQUEST 
// https://expressjs.com/en/4x/api.html#req.params

app.get('/products/:id?', (req, res) => {
    // console.log('+++ ... ',req.params) // {id: '22'}
    // res.send('Guitherehe')
    res.send('Product ID + number ---?> ' + req.params['id'])
})
app.get('/user/:id/name/:idName', (req, res) => { // params yang bisa dikirim == 'id' && 'idName'
    // console.log('== ',req.query)
    res.send('<h3>Data user Baru id name = </h3>' + req.params.idName)
})

// GET /rootUrl?qq=dika+sinaga
// property == 'qq'
// req.query.property = dika+sinaga
app.get('/user', (req, res) => {
    // localhost:3000/user/id=123
    console.log(req.query) // {id: '123}
    res.send(`<h2>Request Query get property </h2> <h3>${req.query.id}</h3>`)
})

app.get('/user/:id', (req, res) => {
    console.log(req.params) // {id:'20'}
    console.log(req.query) // {name: 'dika'}
    res.send(`
    Data user id= ${req.params.id} <br>
    Get query data property ${req.query.name}
    `)
})

// localhost:3000/user/:id
// res = response;
// req = request;
// app.use -> pada tiap path atau pun sembarang path akan tereksekusi
// "localhost:3000/asdXXX" ttp akan running walaupun belum dimasukkan dalam list
// sehingga perlu nampilkan 'pesan' di screen pada sembarang path // misal error 404
app.use('/', (req, res) => { // use harus di posisi akhir sebelum app.listen
    res.status(404)// default bila halaman yang di tulis -> status code "404 not found"
    res.send('<h1>404</h1>');
})
app.listen(port, () => {
    console.log(`Example app listening at ${port}`)
})

// terminal = node app
// http://localhost:3000/
// NODEMON
// start server => nodemon app

// APP.USE
// dok = https://expressjs.com/en/4x/api.html
// app.use([path,]callback[,callback...])

// API REFERENCE 
// https://expressjs.com/en/4x/api.html#res
// misal res.json / req.query dll

// RESPONSE JSON
// res.json mengirimkan response yang mana parameternya dikonversi ke JSON string menggunakan JSON.stringify()
// res.json(null)
// res.json({ user: 'tobi' })
// res.status(500).json({ error: 'message' })

//30.23