// DOCS = https://nodejs.org/dist/latest-v14.x/docs/api/
// create server
// https://nodejs.org/dist/latest-v14.x/docs/api/http.html
// http.createServer([options][, requestListener])

// W3SCHOOL = https://www.w3schools.com/nodejs/nodejs_http.asp

// var http = require('http');

// create server object
// http.createServer((req, res) => {
//     res.write('Tampilan dalam layar port 3000..')
//     res.end()// 
// }).listen(3000,()=>{
//     console.log('server run on port 3000..')
// }) // server object listens on port 3000;

// cara 2   
// const server = http.createServer((req, res) => {
//     res.write('Tampilan dalam layar web port 3001')
//     res.end()// proses berhenti
//  })
// server.listen(3001, () => {
//     console.log('Server is listening on port 3001..')
// })

// cara 3
// http
//     .createServer((req, res) => { 
//         res.write('Tampilan dalam layar on port 3002..')
//         res.end()
//     })
//     .listen(3002, () => {
//         console.log('Server is listening on port 3002..')
//     })

// run di terminal 'node app' untuk tiap cara
// buka browser -> type 'localhost:3000'
// run well


// ADD AN HTTP HEADER --> res.writeHead()
// status code = 200, --> means that all is OK 
// 2nd parameters ; 2nd arguments containing the response headers

// var http = require('http')
// http
// .createServer((req,res)=>{
//     res.writeHead(200, {
//         'Content-Type':'text/html' // the HTTP server is supposed to be displayed as HTML
//     })
//     res.write("run in screen -> content type text/html, code status = 200");
//     res.end()
// })
// .listen(3000, ()=>{
//     console.log('Server is running on port 3000...')
// })

// pada web -> inspect -> tab 'network'
// refresh ulang web
// tab network -> klik 'localhost'
// 'General
// Request URL: http://localhost:3000/
// Request Method: GET
// Status Code: 200 OK
//....

// Response Headers //...
// Content-Type: text/html


// READ THE QUERY STRING
// http://localhost:3000/url_name // get url name // read the query string

// var http = require('http')
// var port = 3000;
// var fs = require('fs');
// http
//     .createServer((req, res) => {
//         const my_url = req.url;
//         // res.write(my_url)
//         // localhost:3000/
//         // my_url = /
//         // localhost:3000/contact
//         // my_url = /contact
//         // localhost:3000/profile
//         // my_url = /profile

//         res.writeHead(200, {
//             'Content-Type': 'text/html'
//         })
//         my_url === '/contact' ?
//             res.write(`<h1>text on the CONTACT screen - server ${port} </h1>`) 
//             :
//             my_url === '/profile' ?
//                 res.write(`<h1>teon the PROFILE screen - server ${port}</h1>`) :
//                 my_url === '/about' ?xt 
//                     res.write(`<h1>text on the ABOUTE screen - server ${port}</h1>`) :
//                     // res.write(`<h1>text on the MAIN screen - server ${port}</h1>`)
//                     // res.write bisa diganti dengan 'fs'
//                     fs.readFile('./index.html', (err, data) => {
//                         if (err) {
//                             res.writeHead(400) // default -> this site can't be reached; localhost refused to connect ...
//                             res.write('Error: file not found')
//                         } else {
//                             res.write(data)
//                         }
//                         res.end()
//                     })

//     })
//     .listen(port, () => {
//         console.log(`server run on port ${port}..`)
//     })

// READ QUERY STRING UPDATE

// var http = require('http');
// var fs = require('fs');
// var port = 3000;

// const renderHTML=(urlPath, res)=>{
//     fs.readFile(urlPath, (err, response) => {
//         if (err) {
//             res.writeHead(400)
//             res.write('Error: the url not found')
//         } else {
//             res.write(response)
//         }
//         res.end()
//     })
// }

// http
//     .createServer((req, res) => {
//         // cek url input
//         res.writeHead(200, {
//             "Content-Type": 'text/html'
//         })
//         const url = req.url;

//         if (url === '/') {
//             fs.readFile('./index.html', (err, response) => {
//                 if (err) {
//                     res.writeHead(400)
//                     res.write('Error: the url not found')
//                 } else {
//                     res.write(response)
//                 }
//                 res.end()
//             })
//             // renderHTML('./index.html', res)
//         }
//     })
//     .listen(3000, () => {
//         console.log(`server run on ${port} port..`)
//     })

//MULTI URL & REUSABLE
var http = require('http')
var fs = require('fs')
var port = 3000

const renderHTML = (urlPath, data) => {
    fs.readFile(urlPath, (err, response) => {
        if (err) {
            data.writeHead(400)
            data.write('Error: the url not found')
        } else {
            data.write(response)
        }
        data.end()
    })
}
http
    .createServer((req, res) => {
        const url = req.url;
        res.writeHead(200, {
            'Content-Type': "text/html"
        })
        if (url === '/') {
            renderHTML('./index.html', res)
        } else if(url == '/about'){
            renderHTML('./index.html', res)
        } else if(url == '/contact'){
            renderHTML('./index.html', res)
        }else if(url == '/profile'){
            renderHTML('./index.html', res)
        } else {
            res.write('<h1>Welcome Home</h1>')
        }
    })
    .listen(3000, () => {
        console.log('server run on port 3000..')
    })