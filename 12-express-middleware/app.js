const express = require('express')
const app = express();
const port = 3000;
const expressLayouts = require('express-ejs-layouts');
var morgan = require('morgan');
// EJS
app.set('view engine', 'ejs')

//THIRD-PARTY MIDDLEWARE
app.use(expressLayouts);

// BUILT-IN MIDDLEWARE
app.use(express.static('public')) // semua data dalam folder public -> bisa diakses
app.use(morgan('dev'))

// APPLICATION-LEVEL MIDDLEWARE
app.use(function (req, res, next) {
    console.log('TIME = ', Date.now())
    next() // bila hilang --> error    
})

app.get('/myJson', (req, res) => {
    res.json({
        "name": "Dika Sinaga",
        "age": "23",
        "email": "mail@example.com"
    })
})

// Application Level Middleware

const student = [
    {
        name: 'Dika Sinaga',
        email: 'mail@email.com'
    },
    {
        name: 'Kipas Dewaa',
        email: 'mail@dewa.com'
    },
    {
        name: 'lenovo asus',
        email: 'mail@em.com'
    },
]
//EJS --> File.ejs --> akses file html
app.get('/', (req, res) => {
    res.render('index', {
        time: Date.now(),
        name: 'Dika Sinaga',
        role: 'Mobile Developer',
        student,
        title: 'Halaman Utama',
        layout: 'layouts/main-layouts'
    })
})

// EJS --> about.ejs
app.get('/about', (req, res, next) => {
    res.render('about', {
        nama: "Nami TEch",
        fungsi: 'fan',
        title: 'Halaman ABOUT',
        layout: 'layouts/main-layouts' // mencari direktori layouts
    })
})

// EJS -> contact.ejs
app.get('/contact', (req, res) => {
    res.render('contact', {
        title: "Halaman CONTACT",
        layout: 'layouts/main-layouts'
    })
})

app.get('/profile', (req, response) => {
    response.send('Hello My confustucated world PROFILE')
    // === response.write di screen
})
//REQUEST 
// https://expressjs.com/en/4x/api.html#req.params

app.get('/products/:id?', (req, res) => {
    // console.log('+++ ... ',req.params) // {id: '22'}
    // res.send('Guitherehe')
    res.send('Product ID + number ---?> ' + req.params['id'])
})
app.get('/user/:id/name/:idName', (req, res) => { // params yang bisa dikirim == 'id' && 'idName'
    // console.log('== ',req.query)
    res.send('<h3>Data user Baru id name = </h3>' + req.params.idName)
})

// GET /rootUrl?qq=dika+sinaga
// property == 'qq'
// req.query.property = dika+sinaga
app.get('/user', (req, res) => {
    // localhost:3000/user/id=123
    console.log(req.query) // {id: '123}
    res.send(`<h2>Request Query get property </h2> <h3>${req.query.id}</h3>`)
})

app.get('/user/:id', (req, res) => {
    console.log(req.params) // {id:'20'}
    console.log(req.query) // {name: 'dika'}
    res.send(`
    Data user id= ${req.params.id} <br>
    Get query data property ${req.query.name}
    `)
})

// localhost:3000/user/:id
// res = response;
// req = request;
// app.use -> pada tiap path atau pun sembarang path akan tereksekusi
// "localhost:3000/asdXXX" ttp akan running walaupun belum dimasukkan dalam list
// sehingga perlu nampilkan 'pesan' di screen pada sembarang path // misal error 404
app.use('/', (req, res) => { // use harus di posisi akhir sebelum app.listen
    res.status(404)// default bila halaman yang di tulis -> status code "404 not found"
    res.send('<h1>404</h1>');
})
app.listen(port, () => {
    console.log(`Example app listening at ${port}`)
})

// terminal = node app
// http://localhost:3000/
// NODEMON
// start server => nodemon app

// APP.USE
// dok = https://expressjs.com/en/4x/api.html
// app.use([path,]callback[,callback...])

// API REFERENCE 
// https://expressjs.com/en/4x/api.html#res
// misal res.json / req.query dll

// RESPONSE JSON
// res.json mengirimkan response yang mana parameternya dikonversi ke JSON string menggunakan JSON.stringify()
// res.json(null)
// res.json({ user: 'tobi' })
// res.status(500).json({ error: 'message' })

//30.23