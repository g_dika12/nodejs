const chalk = require('chalk');
const fs = require('fs');

// membuat folder jika belum ada
const dirPath = './data'
if (!fs.existsSync(dirPath)) {
    fs.mkdirSync(dirPath)
}

// membuat file jika tidak ada
const filePath = './data/contacts.json';
if (!fs.existsSync(filePath)) {
    fs.writeFileSync(filePath, '[]', 'utf-8') // data JSON wajib string
}

const loadContact = () => {
    const contactJSON = fs.readFileSync('./data/contacts.json', 'utf-8');//"[]"
    const newArray = JSON.parse(contactJSON)// create [] or catch data lama dan initial
    return newArray;
}

module.exports = {
    loadContact
}