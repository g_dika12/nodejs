const express = require('express')
const app = express();
const port = 3000;
const expressLayouts = require('express-ejs-layouts');
const { loadContact } = require('./utils/contacts')
// EJS
app.set('view engine', 'ejs')

//THIRD-PARTY MIDDLEWARE
app.use(expressLayouts);

// BUILT-IN MIDDLEWARE
app.use(express.static('public')) // semua data dalam folder public -> bisa diakses

app.get('/myJson', (req, res) => {
    res.json({
        "name": "Dika Sinaga",
        "age": "23",
        "email": "mail@example.com"
    })
})

// Application Level Middleware

const student = [
    {
        name: 'Dika Sinaga',
        email: 'mail@email.com'
    },
    {
        name: 'Kipas Dewaa',
        email: 'mail@dewa.com'
    },
    {
        name: 'lenovo asus',
        email: 'mail@em.com'
    },
]
//EJS --> File.ejs --> akses file html
app.get('/', (req, res) => {
    res.render('index', {
        time: Date.now(),
        name: 'Dika Sinaga',
        role: 'Mobile Developer',
        student,
        title: 'Halaman Utama',
        layout: 'layouts/main-layouts'
    })
})

// EJS --> about.ejs
app.get('/about', (req, res, next) => {
    res.render('about', {
        nama: "Nami TEch",
        fungsi: 'fan',
        title: 'Halaman ABOUT',
        layout: 'layouts/main-layouts' // mencari direktori layouts
    })
})

// EJS -> contact.ejs
app.get('/contact', (req, res) => {
    const contacts = loadContact(); // data object -> parsing json
    res.render('contact', {
        title: "Halaman CONTACT",
        layout: 'layouts/main-layouts',
        contacts,
    })
})

app.use('/', (req, res) => { // use harus di posisi akhir sebelum app.listen
    res.status(404)// default bila halaman yang di tulis -> status code "404 not found"
    res.send('<h1>404</h1>');
})
app.listen(port, () => {
    console.log(`Example app listening at ${port}`)
})
