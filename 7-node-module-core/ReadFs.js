const readline = require('readline');
const fs = require('fs');

const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
})

rl.question("Your Name: ", (name) => {
    rl.question("Age : ", (age) => {
        rl.question("Address: ", (address) => {
            rl.question("Education: ", (education) => {
                rl.question("Job: ", (job) => {
                    const user = {
                        name,
                        age,
                        address,
                        education,
                        job
                    }
                    // read data == fs
                    const store = (fs.readFileSync('./run/user.json', 'utf-8'))
                    // console.log(store)// data string
                    /*
                    [

                    ]
                    */
                    const storeJSON = JSON.parse(store)
                    // pembacaan initial JSON bentuk Array
                    // console.log(storeJSON)// []
                    // update JSON
                    storeJSON.push(user)
                    /*
                    [
                        { name: 'dd', age: 'ff', address: 'gg', education: 'hh', job: 'jj' }
                    ]
                    */
                    // write untuk replace data user.json
                    fs.writeFileSync('./run/user.json',JSON.stringify(storeJSON))
                    console.log('input success')
                    rl.close()
                })
            })
        })
    })
})