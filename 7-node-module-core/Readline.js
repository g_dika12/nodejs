const fs = require('fs');
const readline = require('readline');

const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});

rl.question("Nama ibu kandung Anda : ", (ibu) => {
    rl.question('No Hp ibu Anda : ', (hp) => (
        rl.question("Alamat Rumah : ", (alamat) => {
            const contact = {
                "name": ibu,
                "hp": hp,
                "address": alamat
            };
            const file = fs.readFileSync('./run/testReadline.json', 'utf8')
            /*
            testReadline.json ===
            [
                // initialisasi dengan [ ] // === string
            ]
            */
            const contactJSON = JSON.parse(file) // [] => array kosong
            contactJSON.push(contact) // []
            /*
            [ { name: 'dika', hp: '0899\\', address: 'dfgj' } ]
            */
            // fs.writeFileSync('./run/testReadline.json',contactJSON) // Error -> data yang dimasukkan haruslah STRING
            fs.writeFileSync('./run/testReadline.json', JSON.stringify(contactJSON))
            console.log('Terimakasih sudah memasukkan data.')
            rl.close();

        }
        )
    ))
})